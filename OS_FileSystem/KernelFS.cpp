#include "KernelFS.h"
#include <mutex>
#include "part.h"
#include "BitVektor.h"
#include "IndexAlloc.h"
#include "unordered_map"
#include <iostream>
#include "fileinfo.h"
#include "File.h"
#include <string>

std::mutex partitionMutex, fileOpen, openedFiles, cs;
std::condition_variable cv;
int KernelFS::wr = 0;
int KernelFS::numOfFilesOpened = 0;
int KernelFS::numOfClusters = 0;
Partition* KernelFS::myPartition = nullptr;
char* KernelFS::bitVector = { 0 };
std::unordered_map<std::string, FileInfo> KernelFS::umap;
char* KernelFS::currFile = 0;
char KernelFS::mod = 0;
FileInfo* KernelFS::fi = nullptr;


char KernelFS::mount(Partition* partition)
{
	if (!partition) return 0;
		
	//	partitionMutex.lock();
		umap.clear();
		BitVektor::load(partition);
		myPartition = partition;

	return 1;
}

char KernelFS::unmount()
{
	std::unique_lock<std::mutex> lk(openedFiles);
	cv.wait(lk, [] {return numOfFilesOpened == 0; });

	myPartition = nullptr;
	umap.clear();
//	partitionMutex.unlock();
	
	return 0;
}

char KernelFS::format()
{
	std::unique_lock<std::mutex> lk(openedFiles);
	cv.wait(lk, [] {return numOfFilesOpened == 0; });
	umap.clear();
	BitVektor::format();
	IndexAlloc::format(myPartition);

	return 0;
}

FileCnt KernelFS::readRootDir()
{
	return umap.size();
}

char KernelFS::doesExist(char* fname)
{
	if (umap.find(fname) == umap.end())
		return 0;
	else
		return 1;
}


File* KernelFS::open(char* fname, char mode)
{

	if (mode == 'r' || mode  == 'a' ) {
		if (!doesExist(fname)) {
		//	openingFile.unlock();
			return 0;
		}

		while (KernelFS::wr > 0) {
			//blokiraj se
		//	std::cout << "blokiraj se";
		}
		//open clusters for info and data
		//fileinfo.numOfReaders++;
		//if (mode == 'a') fileinfo.numOfWriters++;
		currFile = fname;
		mod = mode;
		numOfFilesOpened++;
		FileInfo fileinfo = (umap.find(fname))->second;
		
	//	KernelFS::fi = &fileinfo;
		return new File(fname, fileinfo.fileCluster, fileinfo.firstLevelIndex, fileinfo.secondLevelIndex,fileinfo.readFromCluster, fileinfo.getMode(), fileinfo.getSize(), fileinfo.getNumOfReaders(), fileinfo.getNumOfWriters());
		return new File();
	} 
	else if (mode == 'w') {
		currFile = fname;
		mod = mode;
		numOfFilesOpened++;
		KernelFS::wr++;
		IndexAlloc::allocNewForIndex(myPartition, fname);
		FileInfo fileinfo(fname, mode,myPartition);					//takodje obrisi postojeci fajl 
		auto inserted = umap.insert(std::make_pair(fname, fileinfo));
		char* buffer = new char[ClusterSize];
		for (int i = 0; i < ClusterSize; i++) buffer[i] = 0;
		for (int i = 0; i < 4; i++) {
			buffer[i] = fileinfo.secondLevelIndex >> (i * 8);
		}
		myPartition->writeCluster(fileinfo.firstLevelIndex, buffer);
		for (int i = 0; i < 4; i++) {
			buffer[i] = fileinfo.fileCluster >> (i * 8);
		}
		myPartition->writeCluster(fileinfo.secondLevelIndex, buffer);
		for (int i = 0; i < 4; i++) buffer[i] = 0;
		myPartition->writeCluster(fileinfo.fileCluster, buffer);
		KernelFS::fi = &inserted.first->second;

		delete buffer;
		return new File();
	} 

	
	return nullptr; //greska
}

char KernelFS::deleteFile(char* fname)
{
	if(--numOfFilesOpened==0) filesClosed();
		KernelFS::wr--;
	return 0;
}

void KernelFS::filesClosed()
{
	numOfFilesOpened = 0;
	cv.notify_one();
}

Partition* KernelFS::getPartition()
{
	return myPartition;
}

void KernelFS::find(char* filename, char** fname, char** ext)
{
	char* name = new char[8];
	char* ex = new char[3];
	int i = 0;
	while (filename[i] != '\0') {

		if (filename[i] == '/') {
			int j = 0; i++;
			while (filename[i] != '/' && filename[i] != '.') {
				name[j++] = filename[i];
				i++;
			}
		}

		if (filename[i] == '.') {
			int j = 0; i++;
			while (filename[i] != '\0') {
				ex[j++] = filename[i];
				i++;
			}
		}
	}
	*fname = name;
	*ext = ex;
}
