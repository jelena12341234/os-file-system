#ifndef KERNEL_FS
#define KERNEL_FS
#include <unordered_map> 


typedef long FileCnt;
class File;
class Partition;
typedef unsigned long ClusterNo;
class FileInfo;
class KernelFile;

class KernelFS {
private:
	friend class KernelFile;
	static Partition* myPartition;
	static int numOfFilesOpened, numOfClusters;
	static char* bitVector;
	static ClusterNo* indexFirstRoot;
	static std::unordered_map<std::string , FileInfo> umap;
public:
	static int wr;
	static FileInfo* fi;
	static char* currFile;
	static char mod;
	static Partition* getPartition();
	static void find(char* filename, char** fname, char** ext);
	static char mount(Partition* partition); //montira particiju
		// vraca 0 u slucaju neuspeha ili 1 u slucaju uspeha
	static char unmount(); //demontira particiju
	// vraca 0 u slucaju neuspeha ili 1 u slucaju uspeha
	static char format(); //formatira particiju;
	// vraca 0 u slucaju neuspeha ili 1 u slucaju uspeha
	static FileCnt readRootDir();
	// vraca -1 u slucaju neuspeha ili broj fajlova u slucaju uspeha
	static char doesExist(char* fname); //argument je naziv fajla sa
	//apsolutnom putanjom
	static File* open(char* fname, char mode);
	static char deleteFile(char* fname);
	static void filesClosed();
};


#endif // !1