#include "BitVektor.h"
#include "IndexAlloc.h"

Partition* BitVektor::myPartition = nullptr;
long BitVektor::numOfBits = 0;
char* BitVektor::bitVector = { 0 };
int BitVektor::clustersForBitVector = 0;

void BitVektor::load(Partition* partition)
{
	if (!partition || myPartition) return;
	myPartition = partition;
	delete bitVector;
	bitVector = nullptr;

	int numOfClusters = myPartition->getNumOfClusters();
	

	if (numOfClusters % ClusterSizeInBits == 0)
		clustersForBitVector = numOfClusters / ClusterSizeInBits;
	else
		clustersForBitVector = numOfClusters / ClusterSizeInBits + 1;
	//ako ima 25 klastera
	//za njih koristim 3 chara + 1 
	int numOfBytes = numOfClusters / 8; 
	if (numOfClusters % 8 != 0) numOfBytes++;
	
	bitVector = new char[clustersForBitVector*ClusterSize];


	for (int i = 0; i < clustersForBitVector; i++)
	{
		//ako 8 klastera stavim u jednu adresu
		//onda cu 2048 da stavim u 2048/8
		myPartition->readCluster(i, bitVector + i * ClusterSize );
	}


	IndexAlloc::loadIndexRootDir(BitVektor::clustersForBitVector, partition,clustersForBitVector + 1);

}

void BitVektor::format()
{

	int	numOfClusters = myPartition->getNumOfClusters();
	long neededBytes = numOfClusters / 8;
	if (neededBytes * 8 != numOfClusters) neededBytes++;
	unsigned char* buffer = new unsigned char[ClusterSize];
	//1 su slobodni blokovi, 0 su zauzeti, sada je samo prvi zauzet za bit vektor
	unsigned char firstClusterTaken = 0xFF;

	for (int i = 0; i <= clustersForBitVector; i++) { // for rootdir
		firstClusterTaken = firstClusterTaken >> 1;
	}
/*
	if (numOfClusters % ClusterSizeInBits == 0)
		clustersForBitVector = numOfClusters / ClusterSizeInBits;
	else
		clustersForBitVector = numOfClusters / ClusterSizeInBits + 1;
		
		isti je broj kao i kod mount()
		*/ 
	buffer[0] = firstClusterTaken;
	for (int i = 1; i < neededBytes; i++) buffer[i] = 0xFF;
	delete bitVector;
	bitVector =(char*)buffer;

	for (int i = 0; i < clustersForBitVector; i++) {

		myPartition->writeCluster(i, (char*)buffer + i*ClusterSize);
	}

	
}

ClusterNo BitVektor::findFirstFree()
{
	int top = myPartition->getNumOfClusters() / 8;
	if (myPartition->getNumOfClusters() % 8 != 0) top++;
	int tst = 0b001111111;
	for (int i = 0; i < top; i++) 
	{
		if (bitVector[i] & 1) {
			int freeClust = 0x80;
			for (int j = 0;j < 8; j++)
			{
				if (bitVector[i] & freeClust)
				{

					bitVector[i] = bitVector[i] >> 1;
					bitVector[i] = bitVector[i] & tst;
					return 8 * i + j;
				}
				freeClust = freeClust >> 1;
			}
	}
	}
	return ClusterNo();
}


/*
ClusterNo KernelFS::findFirstFreeCluster()
{
	int numCl = myPartition->getNumOfClusters();
	for (int i = 0, int num = numCl % 8 == 0 ? numCl / 8 : numCl / 8 + 1; i < num; i++) {
		if (bitVector[i] & 1) //ima slobodnih u ovom delu
		{
			int freeClust = 0x80;
			for (int j = 1; j <= 8; j++)
			{
				if (bitVector[i] & freeClust) return 8 * i + j;
				freeClust = freeClust >> 1;
			}
		}
	}
	return -1;
}

*/