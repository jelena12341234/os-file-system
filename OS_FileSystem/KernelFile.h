#pragma once

typedef unsigned long BytesCnt;
typedef unsigned long ClusterNo;
class FileInfo;

class KernelFile
{
private:
	FileInfo* myFile;
	bool isOpen;
	BytesCnt size, pos;
public:
	KernelFile(char*, ClusterNo fileC, ClusterNo firstC, ClusterNo secondC, ClusterNo readC, char mod, unsigned long sz, int readers, int writers);
	KernelFile();
	~KernelFile();
	char write(BytesCnt, char* buffer);
	BytesCnt read(BytesCnt, char* buffer);
	char seek(BytesCnt);
	BytesCnt filepos();
	char eof();
	BytesCnt getFileSize();
	char truncate();
	bool isOpened();
};

