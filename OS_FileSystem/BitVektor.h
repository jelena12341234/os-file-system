#pragma once
#include "part.h"

constexpr long ClusterSizeInBits = 2048 * 8;

class BitVektor
{
	static Partition* myPartition;
	static long numOfBits;
	static char* bitVector;
public:
	static int clustersForBitVector;
	//bice samo jedna particija 
	//tako da mogu biti staticka polja i metode

	static void load(Partition* partition);
	static void format();
	static ClusterNo findFirstFree();
};
