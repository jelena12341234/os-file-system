#pragma once
class KernelFile;
typedef unsigned long BytesCnt;
typedef unsigned long ClusterNo;

class File
{
public:
	~File();
	char write(BytesCnt, char* buffer);
	BytesCnt read(BytesCnt, char* buffer);
	char seek(BytesCnt);
	BytesCnt filepos();
	char eof();
	BytesCnt getFileSize();
	char truncate();
private:
	friend class FS;
	friend class KernelFS;
	File(char*,ClusterNo, ClusterNo, ClusterNo, ClusterNo, char, unsigned long, int, int);

	File();
	KernelFile* myImpl;

};

