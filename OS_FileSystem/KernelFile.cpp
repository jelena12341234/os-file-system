#include "KernelFile.h"
#include <iostream>
#include "KernelFS.h"
#include "File.h"
#include "fileinfo.h"
#include "IndexAlloc.h"
#include <string>

KernelFile::KernelFile(char* name, ClusterNo fileC, ClusterNo firstC, ClusterNo secondC, ClusterNo readC, char mod, unsigned long sz, int readers, int writers)
{
	isOpen = true;
	myFile = new FileInfo(name, fileC, firstC, secondC, readC, mod, sz, readers, writers);
	pos = 0;
	size = sz;
}

KernelFile::KernelFile()
{
	myFile = KernelFS::fi;
	myFile->myPartition = KernelFS::getPartition();
	if (myFile->getMode() == 'w')
		myFile->numOfWriters++;
	size = myFile->getSize();
	myFile->name = KernelFS::currFile;
	isOpen = true;
	pos = 0;
}

KernelFile::~KernelFile()
{
	if (myFile->getMode() == 'w')
		myFile->numOfWriters--;
	isOpen = false;
	FileInfo f(myFile->name, myFile->fileCluster, myFile->firstLevelIndex, myFile->secondLevelIndex, myFile->readFromCluster, myFile->getMode(), myFile->getSize(), myFile->numOfReaders, myFile->numOfWriters);
	char* name = myFile->name;
	KernelFS::umap.erase(myFile->name);
	KernelFS::umap.insert(std::make_pair(name, f));
	KernelFS::deleteFile(myFile->name);
}

char KernelFile::write(BytesCnt cnt, char* buffer)
{
	if (buffer == nullptr && cnt > 0) return 0;
	if (buffer == nullptr || cnt == 0) return 1;	

	char* buff = new char[ClusterSize];

	myFile->myPartition->readCluster(myFile->fileCluster, buff);
	int startPoint = size % ClusterSize;
	bool* bl = new bool[ClusterSize];
	for (int i = 0; i < ClusterSize; i++) {
		if (pos % 2048 >= i && pos!=0 && pos!=1 && pos!=2) bl[i] = true;
		else bl[i] = false;
	}


	for (startPoint = 0; startPoint < ClusterSize; startPoint++) {
		if (buff[startPoint] == 0 && !bl[startPoint]) break;
	}
	
	if (pos % 2048 != startPoint) for (; startPoint < ClusterSize; startPoint++) {
		if (buff[startPoint] == 0) break;
	}


	int bytesTransfered = 0;

	while (bytesTransfered < cnt) {


		for (; bytesTransfered < cnt && startPoint < ClusterSize; bytesTransfered++) {
			buff[startPoint++] = buffer[bytesTransfered];
			pos++;
			size++;
			myFile->setSize(size);
		}

		if(bytesTransfered<cnt) {
			//potrebno zapisati trenutno upisane podatke
			myFile->myPartition->writeCluster(myFile->fileCluster, buff);
			//potrebno je alocirati novi potrebno je da prosledis nivo prvog nivo drugog 
			IndexAlloc::allocNewForData(KernelFS::getPartition(), &myFile);
			startPoint = 0;
			for (int i = 0; i < ClusterSize; i++) buff[i] = 0x00;
		}
	
	}

	myFile->myPartition->writeCluster(myFile->fileCluster, buff);
	delete[] buff;
	delete[] bl;

	return 1;
}

BytesCnt KernelFile::read(BytesCnt cnt, char* buffer)
{

	if (buffer == nullptr && cnt > 0) return 0;
	if (buffer == nullptr || cnt == 0) return 0;
	if (pos == size) return 0;
	std::string result = "";

	BytesCnt res = 0;
	while (res < cnt && pos < size) {
		unsigned long entranceFirst = (pos / (512 * 2048))*4;
			//(pos / ((2048/4) * (2048 / 4)));
		unsigned long entranceSecond = (pos / 2048)*4;
		unsigned long entranceFile = pos % 2048;

		char* flvl = new char[ClusterSize];
		char* slvl = new char[ClusterSize];
		char* filelvl = new char[ClusterSize];
		myFile->myPartition->readCluster(myFile->firstLevelIndex, flvl);
		ClusterNo scndId = 0;
		for (int j = 0; j < 4; j++) scndId |=(unsigned char)flvl[entranceFirst++] << (j * 8);
		myFile->myPartition->readCluster(scndId, slvl);

		ClusterNo fId = 0;
		for (int j = 0; j < 4; j++) fId |= (unsigned char)slvl[entranceSecond++] << (j * 8);
		myFile->myPartition->readCluster(fId, filelvl);


		for (int i = 0; i < cnt && pos < size && res<cnt && entranceFile < ClusterSize; i++) {
			result.push_back(filelvl[entranceFile++]);
			res++;
			pos++;
		}

		delete[] flvl;
		delete[] slvl;
		delete[] filelvl;
	}
	memcpy(buffer, result.c_str(), result.length());

	return res;
}

char KernelFile::seek(BytesCnt cnt)
{
	if (cnt > size) return 0;
//	IndexAlloc::moveCursor(myFile->myPartition, &myFile, cnt);
	pos = cnt;
	return 1;
}

BytesCnt KernelFile::filepos()
{
	return pos;
}

char KernelFile::eof()
{
	if (pos == size) return 2;
	if (pos != size) return 0;
	return 1;
}

BytesCnt KernelFile::getFileSize()
{
	return size;
}

char KernelFile::truncate()
{
	BytesCnt cnt = size - pos;
	char* buffer = new char[cnt];
	for (int i = 0; i < cnt; i++) buffer[i] = 0x00;

	if (buffer == nullptr && cnt > 0) return 0;
	if (buffer == nullptr || cnt == 0) return 1;

	char* buff = new char[ClusterSize];
	for (int i = 0; i < ClusterSize; i++) buff[i] = 0x00;

	KernelFS::getPartition()->readCluster(myFile->fileCluster, buff);
	int startPoint = pos;

	int bytesTransfered = 0;

	while (bytesTransfered < cnt) {

		for (; bytesTransfered < cnt && startPoint < ClusterSize; bytesTransfered++) {
			buff[startPoint++] = buffer[bytesTransfered];
			pos++;
			size++;
			myFile->setSize(size);
		}

		if (bytesTransfered < cnt) {
			//potrebno zapisati trenutno upisane podatke
			KernelFS::getPartition()->writeCluster(myFile->fileCluster, buff);
			//potrebno je alocirati novi potrebno je da prosledis nivo prvog nivo drugog 
			IndexAlloc::allocNewForData(KernelFS::getPartition(), &myFile);
			startPoint = 0;
			for (int i = 0; i < ClusterSize; i++) buff[i] = 0x00;
		}

	}

	KernelFS::getPartition()->writeCluster(myFile->fileCluster, buff);
	return 1;
}

bool KernelFile::isOpened()
{
	return isOpen;
}
