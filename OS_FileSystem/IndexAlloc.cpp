#include "IndexAlloc.h"
#include "part.h"
#include "BitVektor.h"
#include "fileinfo.h"
#include "KernelFS.h"
#include "iostream"

char* IndexAlloc::firstLevelIndex = { 0 };
ClusterNo IndexAlloc::lastSecondIndex = 0;
ClusterNo IndexAlloc::lastSecondInfo = 0, IndexAlloc::indexForFileInfo = 0;
ClusterNo IndexAlloc::lastFirstInfo = 0;
char* IndexAlloc::secondIndex = { 0 };
char* IndexAlloc::firstInfo = { 0 };
char* IndexAlloc::secondInfo = { 0 }, *IndexAlloc::fileInfo = { 0 };
unsigned long IndexAlloc::numOfDesc = 0; //points to the last place in fileInfo
unsigned int IndexAlloc::entriesTakenFIf = 0, IndexAlloc::entriesTakenFIx = 0, IndexAlloc::entriesTakenSIf = 0, IndexAlloc::entriesTakenSIx=0;


void IndexAlloc::loadIndexRootDir(ClusterNo rootIndex, Partition* partititon, ClusterNo firstIndex)
{
	firstLevelIndex = new char[ClusterSize];
	partititon->readCluster(rootIndex, firstLevelIndex);
}


void IndexAlloc::format(Partition* partition)
{
	delete firstLevelIndex;
	firstLevelIndex = new char[ClusterSize];
	char* buff = new char[ClusterSize];
	for (int i = 0; i < ClusterSize ; i++) {
		firstLevelIndex[i] = 0x00;
		buff[i] = 0x00;
	}
	unsigned long num = BitVektor::clustersForBitVector + 1;
	for (int i = 0; i < 4; i++)  buff[i] = num >> (i * 8);

	partition->writeCluster((ClusterNo)BitVektor::clustersForBitVector, buff);
	BitVektor::findFirstFree();
	partition->writeCluster(num, firstLevelIndex);

}

ClusterNo IndexAlloc::allocNewForIndex(Partition* partition, char* filename)
{
	if (!partition) return 0;
//	if(entriesTakenFIx == 2048/4) { firstLevelIndex=new char[ClusterSize]; for(int i=0; i<ClusterSize; i++) firstLevelIndex[i] = 0x00; entriesTakenFIx=0;

	if (secondIndex == 0 || entriesTakenSIx == 2048/4) {
		secondIndex = new char[ClusterSize];
		entriesTakenSIx = 0;
		lastSecondIndex = BitVektor::findFirstFree(); //finds free cluster for second level index
		for (int i = 0; i < ClusterSize; i++) secondIndex[i] = 0x00;
		partition->writeCluster(lastSecondIndex, secondIndex);
	}
		
	if (entriesTakenSIx == 0) {			//if second level index was full, new one is allocated
		ClusterNo ch = lastSecondIndex;
		for (int i = entriesTakenFIx; i < entriesTakenFIx + 4; i++) {
			firstLevelIndex[i] = ch;
			ch >>= 8;
		}
		partition->writeCluster(BitVektor::clustersForBitVector + 1, firstLevelIndex);
		entriesTakenFIx += 4;
	}

	if (numOfDesc == 0 || numOfDesc == 2048 / 32) {
		indexForFileInfo = BitVektor::findFirstFree();
		fileInfo = new char[ClusterSize]; 
		for (int i = 0; i < ClusterSize; i++) fileInfo[i] = 0x00;
	
		for (int i = entriesTakenSIx; i < entriesTakenSIx + 4; i++) {
			secondIndex[i] = indexForFileInfo >> (i*8);
		}
		partition->writeCluster(lastSecondIndex, secondIndex);
		entriesTakenSIx += 4;
	}

	char* mode = new char[3];
	char* name = new char[8];
	KernelFS::find(filename, &name, &mode);
	char* fileInfoCluster = new char[32];
	for (int i = 0; i < 32; i++) fileInfoCluster[i] = 0;
	for (int i = 0; i < 8; i++) {		//filename is max 8 chars
		fileInfoCluster[i] = name[i];
		if (name[i] == -51) fileInfoCluster[i] = ' ';
	}
	for (int i = 8; i < 8+3; i++) {
		fileInfoCluster[i] = mode[i-8]; 
	}
	fileInfoCluster[11] = 0;
	ClusterNo ret = BitVektor::findFirstFree();
	unsigned long r = ret;
	for (int i = 12; i < 16; i++)fileInfoCluster[i] = r >> ((i-12)*8);
	

	for (int i = 0; i < 32; i++) {
		fileInfo[i + 32 * numOfDesc] = fileInfoCluster[i];
	}
	partition->writeCluster(indexForFileInfo, fileInfo);

	numOfDesc++;

	return ret;
}

void IndexAlloc::allocNewForData(Partition* partition, FileInfo** fi)
{
	if (!partition) return;
	char* buff = new char[ClusterSize];
	partition->readCluster((*fi)->secondLevelIndex, buff);
	bool allocated = false;

	while (!allocated) {
		int i = 0;
		for (; i < ClusterSize; i += 4) {
			if (buff[i] == 0) break;
		}

		if (i < ClusterSize) {
			(*fi)->fileCluster = BitVektor::findFirstFree();
			for (int j = 0; j < 4; j++)	buff[i++] = (*fi)->fileCluster >> (j * 8);
			partition->writeCluster((*fi)->secondLevelIndex, buff);
			allocated = true;
			break;
		}
		//alocate first level index
		char* firstlvl = new char[ClusterSize];
		partition->readCluster((*fi)->firstLevelIndex, firstlvl);
		int entries = 0;
		for (; entries < ClusterSize; entries += 4) if (firstlvl[entries] != 0) break;
		if (entries >= ClusterSize) { std::cout << "greska pri alociranju za podatke" << std::endl; return; }
		(*fi)->secondLevelIndex = BitVektor::findFirstFree();
		for (int j = 0; j < 4; j++)	firstlvl[entries++] = (*fi)->secondLevelIndex >> (j * 8);
		partition->writeCluster((*fi)->firstLevelIndex, firstlvl);
		partition->readCluster((*fi)->secondLevelIndex, buff);
	}
	
}
