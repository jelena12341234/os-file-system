#pragma once
#include "BitVektor.h"
#include "KernelFS.h"

typedef unsigned long ClusterNo;

class FileInfo {
	char mode;
	unsigned long size;
public:
	char* name;
	Partition* myPartition;
	unsigned int numOfWriters;
	unsigned int numOfReaders;
	ClusterNo fileCluster, firstLevelIndex, secondLevelIndex, readFromCluster;

	FileInfo(const FileInfo& fi) {
		firstLevelIndex = fi.firstLevelIndex;
		secondLevelIndex = fi.secondLevelIndex;
		fileCluster = fi.fileCluster;
		mode = fi.mode;
		name = fi.name;
		myPartition = fi.myPartition;
		size = fi.size;
		readFromCluster = fi.readFromCluster;
		numOfWriters =fi.numOfWriters;
		numOfReaders =fi.numOfReaders;	
	}
	
	void operator==(FileInfo fi) {
		firstLevelIndex = fi.firstLevelIndex;
		secondLevelIndex = fi.secondLevelIndex;
		fileCluster = fi.fileCluster;
		mode = fi.mode;
		myPartition = fi.myPartition;
		size = fi.size;
		readFromCluster = fi.readFromCluster;
		numOfWriters = fi.numOfWriters;
		numOfReaders = fi.numOfReaders;

	}
	FileInfo(char* nam, ClusterNo fileC, ClusterNo fC, ClusterNo sC, ClusterNo readFrom, char mod, unsigned long sz, int r, int w) {
		fileCluster = fileC;
		firstLevelIndex = fC;
		secondLevelIndex = sC;
		readFromCluster = readFrom;
		mode = mod;
		size = sz;
		numOfReaders = r;
		numOfWriters = w;
		name = nam;
		myPartition = KernelFS::getPartition();

	}

	FileInfo(char* fname, char mod, Partition* p) {
		firstLevelIndex = BitVektor::findFirstFree();
		secondLevelIndex = BitVektor::findFirstFree();
		fileCluster = BitVektor::findFirstFree();
		mode = mod;
		name = fname;
		myPartition = p;
		size = 0;
		readFromCluster = 0;
		numOfWriters = 0;
		numOfReaders = 0;
	}

	ClusterNo getFileClsuter() {
		return fileCluster;
	}

	ClusterNo getSecondLevelIndex() {
		return secondLevelIndex;
	}

	char getMode() {
		return mode;
	}

	void setSize(unsigned long sz) {
		size = sz;
	}

	unsigned long getSize() {
		return size;
	}

	unsigned int getNumOfWriters() {
		return numOfWriters;
	}

	unsigned int getNumOfReaders() {
		return numOfReaders;
	}
};