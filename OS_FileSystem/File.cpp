#include "File.h"
#include "KernelFile.h"

File::File() 
{
	myImpl = new KernelFile();
}

File::File(char* n, ClusterNo fileC, ClusterNo firstC, ClusterNo secondC, ClusterNo readC, char mod, unsigned long size, int readers, int writers)
{
	myImpl = new KernelFile(n,fileC, firstC, secondC, readC, mod,size,readers,writers);
}

File::~File()
{
	delete myImpl;
}

char File::write(BytesCnt cnt, char* buffer)
{
	return myImpl->write(cnt,buffer);
}

BytesCnt File::read(BytesCnt cnt, char* buffer)
{
	return myImpl->read(cnt, buffer);
}

char File::seek(BytesCnt cnt)
{
	return myImpl->seek(cnt);
}

BytesCnt File::filepos()
{
	return myImpl->filepos();
}

char File::eof()
{
	return myImpl->eof();
}

BytesCnt File::getFileSize()
{
	return myImpl->getFileSize();
}

char File::truncate()
{
	return myImpl->truncate();
}
