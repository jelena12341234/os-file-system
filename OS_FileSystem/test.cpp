#include <iostream>
#include "part.h"
#include "fs.h"
#include "resource.h"
#include "BitVektor.h"
#include <fstream>
#include "IndexAlloc.h"
#include "KernelFS.h"
#include "File.h"
#include <assert.h>
#include <iostream>

#define _CRT_SECURE_NO_WARNINGS
using namespace std;

int main1() {

	char* name1 = (char*)"C:\\Users\\jrado\\Downloads\\javniTest_2020\\resources\\p1.ini";
	
	Partition* p = new Partition(name1);
	
	
	FS::mount(p);
	FS::format();

	char* ulazBuffer;
	int ulazSize;

	FILE* fl = fopen("ulaz.dat.jpg", "rb");
	if (fl == 0) {
		std::cout << "GRESKA: Nije nadjen ulazni fajl 'ulaz.dat' u os domacinu!" << std::endl;
		system("PAUSE");
		return 0;//exit program
	}
	ulazBuffer = new char[32 * 1024 * 1024];//32MB
	ulazSize = fread(ulazBuffer, 1, 32 * 1024 * 1024, fl);
	fclose(fl);

	char filepath[] = "/fajl1.dat";
	File* f = FS::open(filepath, 'w');
	 cout <<  ": Kreiran fajl '" << filepath << "'" << endl; 
	f->write(ulazSize, ulazBuffer);
	cout << ": Prepisan sadrzaj 'ulaz.dat' u '" << filepath << "'" << endl; 
	delete f;
	cout << "zatvoren fajl" << filepath << endl;


	f = FS::open(filepath, 'r');
	cout << ": Otvoren fajl " << filepath << "" << endl; 
	ofstream fout("izlaz1.dat", ios::out | ios::binary);
	char* buff = new char[f->getFileSize()];
	f->read(f->getFileSize(), buff);
	fout.write(buff, f->getFileSize());
	cout << ": Upisan '" << filepath << "' u fajl os domacina 'izlaz1.dat'" << endl; 
	delete[] buff;
	fout.close();
	delete f;

	/*



	char* ch = new char[50050];
	for (int i = 0; i < 50050; i++) {
		ch[i] = 65 + i%30;
		cout << ch[i] << " ";
	}
	cout << endl;
	char filepath[] = "/fajl1.dat";
	File* f = FS::open(filepath, 'w');
//	wait(mutex); cout << threadName << ": Kreiran fajl '" << filepath << "'" << endl; signal(mutex);
	f->write(50050, ch);
//	wait(mutex); cout << threadName << ": Prepisan sadrzaj 'ulaz.dat' u '" << filepath << "'" << endl; signal(mutex);
	delete f;
//	wait(mutex); cout << threadName << ": zatvoren fajl '" << filepath << "'" << endl; signal(mutex);

	char* cha = new char[50050];
	File* f1 = FS::open(filepath, 'r');
	cout << "ispis procitanih slovaa" << endl;
	f1->read(50050, cha);
	for (int i = 0; i < 50050; i++) {
		char c =cha[i];
		assert(cha[i] == ch[i]);
		cout << c << " ";
	}

	delete f1;
	*/
	FS::unmount();

	std::cout << "ok" << std::endl ;
	return 0;
}
