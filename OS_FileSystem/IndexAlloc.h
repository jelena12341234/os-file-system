#pragma once

class FileInfo;
class Partition;
typedef unsigned long ClusterNo;
typedef unsigned long BytesCnt;

class IndexAlloc
{
	static ClusterNo* rootDir;
	static char* firstLevelIndex;
	static ClusterNo lastSecondIndex;	//za indekse
	static char* secondIndex, * firstInfo, * secondInfo, *fileInfo;
	static unsigned long numOfDesc;
	static unsigned int entriesTakenFIx, entriesTakenSIx, entriesTakenSIf, entriesTakenFIf;
	static ClusterNo lastSecondInfo; //za podatke
	static ClusterNo lastFirstInfo, indexForFileInfo;
public:
	static void loadIndexRootDir(ClusterNo rootIndex, Partition*,ClusterNo);
	static void format(Partition* partition);
	static void allocNewForData(Partition* partition, FileInfo** fi );
	static ClusterNo allocNewForIndex(Partition* partition, char* filename);
};

